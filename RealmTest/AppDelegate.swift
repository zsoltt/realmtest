//
//  AppDelegate.swift
//  RealmTest
//
//  Created by Zsolt Szatmari on 31.08.17.
//  Copyright © 2017 Zsolt Szatmari. All rights reserved.
//

import Cocoa
import RealmSwift

class Something : Object
{
    dynamic var id = UUID().uuidString
    
    override static func primaryKey() -> String
    {
        return "id"
    }
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        // Insert code here to initialize your application
    
        let realm = try! Realm()
        let object = realm.object(ofType: Something.self, forPrimaryKey: "bogus")
        Swift.print("found object: \(object)")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

